const path = require('path');

module.exports = {
  entry: {
    runner: './src/runner_browser.js',
    bench_js: './src/js/index.js',
  },
  output: {
    filename: '[name]-browser.js',
    library: '[name]',
  },
  resolve: {
    alias: {
      readFile$: path.resolve(__dirname, 'src/read_file_browser.js'),
    },
  },
  externals: [
    (context, request, callback) => {
      if (/bench_js.js$/.test(request) || /bench_cpp.js$/.test(request)) {
        return callback(null, `commonjs ${request}`);
      }
      callback();
    },
  ],
};
