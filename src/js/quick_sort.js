/* eslint-disable import/prefer-default-export, no-plusplus, no-param-reassign */
export function quickSort(array, begin, end, cmp) {
  let pivot;
  let tmp;
  let l;
  let r;
  let p;

  while (begin < end) { // second recursive call
    l = begin;
    p = begin + Math.floor((end - begin) / 2);
    r = end;
    pivot = array[p];

    while (true) {
      while (l <= r && cmp(array[l], pivot) <= 0) {
        ++l;
      }
      while (l <= r && cmp(array[r], pivot) > 0) {
        --r;
      }
      if (l > r) {
        break;
      }
      tmp = array[l];
      array[l] = array[r];
      array[r] = tmp;
      if (p === r) {
        p = l;
      }
      ++l;
      --r;
    }

    array[p] = array[r];
    array[r] = pivot;
    --r;

    // first recursion on shorter side
    if (r - begin < end - l) {
      quickSort(array, begin, r, cmp);
      begin = l;
    } else {
      quickSort(array, l, end, cmp);
      end = r;
    }
  }
}
