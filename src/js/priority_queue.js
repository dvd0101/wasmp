/* eslint-disable no-underscore-dangle, import/prefer-default-export, no-param-reassign */
/* eslint-disable prefer-destructuring */

export class PriorityQueue {
  constructor(cmp) {
    this._heap = [];
    this._cmp = cmp;
    this._heapError = new Error('Heap inconsistent');
  }

  clear() {
    this._heap = [];
  }

  isEmpty() {
    return this._heap.length === 0;
  }

  size() {
    return this._heap.length;
  }

  peek() {
    if (this.isEmpty()) {
      throw this._heapError;
    }
    return this._heap[0];
  }

  push(o) {
    this._heap.push(o);
    this._bubbleUp();
  }

  pop() {
    if (this._heap.length === 0) {
      throw this._heapError;
    } else if (this._heap.length === 1) {
      return this._heap.pop();
    }
    const c = this._heap.length - 1;
    const o = this._heap[0];
    this._heap[0] = this._heap[c];
    this._heap.pop();
    this._bubbleDown();
    return o;
  }

  replace(o) {
    if (this._heap.length === 0) {
      throw this._heapError;
    }
    const x = o;
    o = this._heap[0];
    this._heap[0] = x;
    if (this._heap.length !== 1) {
      this._bubbleDown();
    }
    return o;
  }

  _bubbleUp() {
    let c = this._heap.length - 1;
    let p = Math.floor((c - 1) / 2);
    while (c > 0) {
      if (this._cmp(this._heap[p], this._heap[c]) <= 0) {
        break;
      }
      const x = this._heap[c];
      this._heap[c] = this._heap[p];
      this._heap[p] = x;
      c = p;
      p = Math.floor((c - 1) / 2);
    }
  }

  _bubbleDown() {
    let p = 0;
    let c = 1;

    if (this._heap.length > 2 && this._cmp(this._heap[1], this._heap[2]) > 0) {
      c = 2;
    }
    while (c < this._heap.length) {
      if (this._cmp(this._heap[p], this._heap[c]) <= 0) {
        break;
      }
      const x = this._heap[c];
      this._heap[c] = this._heap[p];
      this._heap[p] = x;
      p = c;
      c = (2 * p) + 1;
      if (c + 1 < this._heap.length && this._cmp(this._heap[c], this._heap[c + 1]) > 0) {
        c += 1;
      }
    }
  }
}

