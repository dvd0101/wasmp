export function f() {}

export function sumNumbers(a, b) { return a + b; }

export function factorial(n) {
  return n * (n > 1 ? factorial(n-1) : 1);
};
