import * as three from 'three';

export const Point = three.Vector3;
export const Direction = three.Vector3;

export const L_EPS = 1e-6;
export const L_EPS_SQ = 1e-12;
export const VERTEX_CLOSENESS = 1e-4;
export const VERTEX_CLOSENESS_SQ = 1e-8;

export class Sphere {
  constructor(c, r) {
    this.center = c || new Point(0, 0, 0);
    this.radius = r !== undefined ? r : 1;
  }

  intersectSphere(s) {
    return this.center.distanceTo(s.center) < (this.radius + s.radius);
  }

  intersectPoint(p) {
    return this.center.distanceTo(p) <= this.radius;
  }

  unionSphere(s) {
    const distance = this.center.distanceTo(s.center);
    if (this.radius >= distance + s.radius) {
      return this;
    }
    if (s.radius >= distance + this.radius) {
      return s;
    }
    const c = new Sphere(
      new Point().addVectors(this.center, s.center).multiplyScalar(0.5),
      Math.max(this.radius, s.radius) + (distance / 2),
    );
    return c;
  }
}

export function distanceSphereToPoint(s, p) {
  return s.center.distanceTo(p) - s.radius;
}

export function distanceSphereToSphere(s1, s2) {
  return distanceSphereToPoint(s1, s2.center) - s2.radius;
}

export class Plane {
  constructor(o, d) {
    this.origin = o || new Point(0, 0, 0);
    this.dir = d || new Direction(0, 1, 0);
  }

  closestPoint(p) {
    const dot = new Point().subVectors(p, this.origin).dot(this.dir);
    return new Point().subVectors(p, this.dir.clone().multiplyScalar(dot));
  }
}

export class Segment {
  constructor(p0, p1) {
    this.p0 = p0;
    this.p1 = p1;
  }

  length() {
    return this.p1.distanceTo(this.p0);
  }

  closestParam(point, clamp, eps) {
    // eslint-disable-next-line no-param-reassign
    eps = eps !== undefined ? eps : L_EPS;
    const l = this.length();
    let t = new Point().subVectors(this.p1, this.p0)
      .dot(new Point().subVectors(point, this.p0)) / l;
    if (l < eps) {
      // eslint-disable-next-line no-param-reassign
      eps = l * 0.2;
    }

    if (clamp) {
      if (t < L_EPS) {
        t = 0;
      } else if (t > l - L_EPS) {
        t = l;
      }
    } else {
      if (Math.abs(t) <= L_EPS) { // eslint-disable-line no-lonely-if
        t = 0;
      } else if (Math.abs(l - t) <= L_EPS) {
        t = l;
      }
    }
    return t;
  }

  closestPoint(p, eps) {
    // eslint-disable-next-line no-param-reassign
    eps = eps !== undefined ? eps : L_EPS;
    const l = this.length();
    return this.p0.clone()
      .add(new Point()
        .subVectors(this.p1, this.p0)
        .multiplyScalar(this.closestParam(p, true, eps) / l));
  }
}
