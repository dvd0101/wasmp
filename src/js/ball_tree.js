/* eslint-disable prefer-destructuring, import/prefer-default-export */
/* eslint-disable no-underscore-dangle, no-continue */
import { Sphere, distanceSphereToPoint } from './geo';
import { PriorityQueue } from './priority_queue';
import { quickSort } from './quick_sort';

function computeUnionBall(balls) {
  return balls.reduce((acc, v) => acc.unionSphere(v), balls[0]);
}

function maxSpanCoordinate(balls) {
  if (balls.length === 0) {
    return 0;
  }

  let index = 0;
  const mins = [balls[index].center.x, balls[index].center.y, balls[index].center.z];
  const maxs = [balls[index].center.x, balls[index].center.y, balls[index].center.z];
  for (index += 1; index < balls.length; index += 1) {
    for (let c = 0; c < 3; c += 1) {
      const ball = balls[index];
      if (ball.center.getComponent(c) - ball.radius < mins[c]) {
        mins[c] = ball.center.getComponent(c) - ball.radius;
      }
      if (ball.center.getComponent(c) + ball.radius > maxs[c]) {
        maxs[c] = ball.center.getComponent(c) + ball.radius;
      }
    }
  }
  const spans = [maxs[0] - mins[0], maxs[1] - mins[1], maxs[2] - mins[2]];
  let maxIndex = 0;
  let maxSpan = spans[0];
  if (spans[1] > maxSpan) {
    maxIndex = 1;
    maxSpan = spans[1];
  }
  if (spans[2] > maxSpan) {
    maxIndex = 2;
    maxSpan = spans[2];
  }
  return maxIndex;
}

function discard(queue, n) {
  while (n > 0) {
    queue.pop();
    n -= 1; // eslint-disable-line no-param-reassign
  }
}

function toList(queue) {
  const output = [];
  while (!queue.isEmpty()) {
    output.push(queue.pop());
  }
  return output;
}

export class StaticIndex {
  constructor(elements) {
    this._elements = elements.map(el => ({ el, ball: el.circumsphere() }));
    this._tree = [];

    this._indexElements();
  }

  size() {
    return this._tree.length;
  }

  _indexElements() {
    this._kdConstruction(-1, 0, this._elements.length);
  }

  _kdConstruction(parent, first, last) {
    if (first >= last) {
      throw new Error('assert');
    }

    const elements = this._elements.slice(first, last);
    const balls = elements.map(el => el.ball);
    if (last - first <= 50) {
      this._tree.push({
        type: 'leaf',
        ball: computeUnionBall(balls),
        parent,
        firstLeaf: first,
        lastLeaf: last,
      });
      return this._tree.length - 1;
    }

    const coord = maxSpanCoordinate(balls);
    quickSort(this._elements, first, last - 1, (a, b) => {
      const ac = a.ball.center.getComponent(coord);
      const bc = b.ball.center.getComponent(coord);
      return ac - bc;
    });

    const splitPoint = Math.floor((last + first) / 2);

    this._tree.push({});
    const nodeIndex = this._tree.length - 1;

    const newNode = {
      type: 'branch',
      parent,
    };
    newNode.leftBranch = this._kdConstruction(nodeIndex, first, splitPoint);
    newNode.rightBranch = this._kdConstruction(nodeIndex, splitPoint, last);
    const b1 = this._tree[newNode.leftBranch].ball;
    const b2 = this._tree[newNode.rightBranch].ball;
    newNode.ball = b1.unionSphere(b2);

    this._tree[nodeIndex] = newNode;
    return nodeIndex;
  }

  nearestNeighbors(point, k, maxDistance) {
    // eslint-disable-next-line no-param-reassign
    maxDistance = maxDistance !== undefined ? maxDistance : Infinity;

    const results = new PriorityQueue((a, b) => -(a.distance - b.distance));

    const query = new Sphere(point, Infinity);
    const stack = [0];
    while (stack.length > 0) {
      const nodeIndex = stack.pop();
      const node = this._tree[nodeIndex];
      if (!node.ball.intersectSphere(query)) {
        continue;
      }
      if (node.type === 'leaf') {
        this._addLeafElements(node, point, maxDistance, results);
        if (results.size() > k) {
          discard(results, results.size() - k);
          query.radius = results.peek().distance;
        }
      } else {
        this._selectBranchNode(node, query, stack);
      }
    }

    const output = toList(results);
    output.reverse();
    return output;
  }

  _addLeafElements(node, point, maxDistance, results) {
    this._elements.slice(node.firstLeaf, node.lastLeaf)
      .map((element) => {
        const r = {
          value: element.el,
          closestPoint: element.el.closestPoint(point),
        };
        r.distance = r.closestPoint.distanceTo(point);
        return r;
      })
      .filter(r => r.distance < maxDistance)
      .forEach(e => results.push(e));
  }

  _selectBranchNode(node, query, stack) {
    const distances = [[Infinity, -1], [Infinity, -1]];
    [node.leftBranch, node.rightBranch].forEach((branchIndex, ix) => {
      if (branchIndex !== -1) {
        const ball = this._tree[branchIndex].ball;
        if (ball.intersectSphere(query)) {
          distances[ix][0] = distanceSphereToPoint(ball, query.center);
          distances[ix][1] = branchIndex;
        }
      }
    });

    distances.sort((a, b) => a[0] > b[0]);
    for (let ix = 0; ix < distances.length; ix += 1) {
      if (distances[ix][1] !== -1) {
        stack.push(distances[ix][1]);
      }
    }
  }
}
