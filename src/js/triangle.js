/* eslint-disable prefer-destructuring, import/prefer-default-export */
import {
  Point,
  Plane,
  Segment,
  Sphere,
  L_EPS_SQ,
  VERTEX_CLOSENESS,
  VERTEX_CLOSENESS_SQ,
} from './geo';

export class Triangle {
  constructor(a, b, c) {
    this.a = a;
    this.b = b;
    this.c = c;
  }

  normal() {
    return new Point().subVectors(this.b, this.a)
      .cross(new Point().subVectors(this.c, this.a))
      .normalize();
  }

  asPlane() {
    return new Plane(this.a, this.normal());
  }

  baryCoords(p) {
    const s0 = new Point().subVectors(this.b, this.a);
    const s1 = new Point().subVectors(this.c, this.a);
    const s2 = new Point().subVectors(p, this.a);
    const d00 = s0.dot(s0);
    const d01 = s0.dot(s1);
    const d11 = s1.dot(s1);
    const d20 = s2.dot(s0);
    const d21 = s2.dot(s1);
    const denom = (d00 * d11) - (d01 * d01);
    if (Math.abs(denom) <= L_EPS_SQ) {
      return [-1, -1, -1];
    }
    const z = ((d11 * d20) - (d01 * d21)) / denom;
    const y = ((d00 * d21) - (d01 * d20)) / denom;
    return [1 - y - z, y, z];
  }

  intersectPoint(p) {
    const b = this.baryCoords(p);
    return b[0] >= 0 && b[1] >= 0 && (b[0] + b[1]) <= 1;
  }

  closestPoint(p) {
    const onEdges = [
      new Segment(this.a, this.b).closestPoint(p, VERTEX_CLOSENESS),
      new Segment(this.b, this.c).closestPoint(p, VERTEX_CLOSENESS),
      new Segment(this.c, this.a).closestPoint(p, VERTEX_CLOSENESS),
    ];
    const distances = [
      p.distanceTo(onEdges[0]),
      p.distanceTo(onEdges[1]),
      p.distanceTo(onEdges[2]),
    ];

    let minIndex = 0;
    let minDistance = distances[0];
    if (distances[1] < minDistance) {
      minIndex = 1;
      minDistance = distances[1];
    }
    if (distances[2] < minDistance) {
      minIndex = 2;
      minDistance = distances[2];
    }

    const onEdge = onEdges[minIndex];
    const projected = this.asPlane().closestPoint(p);
    if (this.intersectPoint(projected) && p.distanceToSquared(onEdge) > VERTEX_CLOSENESS_SQ) {
      return p;
    }
    return onEdge;
  }

  circumsphere() {
    const b = new Sphere(new Point()
      .addVectors(this.a, this.b)
      .add(this.c)
      .divideScalar(3));
    b.radius = Math.max(
      b.center.distanceTo(this.a),
      Math.max(
        b.center.distanceTo(this.b),
        b.center.distanceTo(this.c),
      ),
    );
    return b;
  }
}
