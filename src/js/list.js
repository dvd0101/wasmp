export function sumOfInts(ints) {
  let s = 0;
  for (let ix = 0; ix < ints.length; ix++) {
    s += ints[ix];
  }
  return s;
}

export function sumOfIntsSlow(ints) {
  return ints.reduce((acc, v) => acc + v);
}

export function sumOfIntsBuffer(ints) {
  // copied to not fool the jit
  let s = 0;
  for (let ix = 0; ix < ints.length; ix++) {
    s += ints[ix];
  }
  return s;
}
