#pragma once
#include <Eigen/Dense>
#include <iosfwd>

namespace ball_tree {

	using point = Eigen::Vector3d;
	using direction = Eigen::Vector3d;

	float const l_eps = 1e-6;
	float const l_eps_sq = 1e-12;
	float const vertex_closeness = 1e-4;
	float const vertex_closeness_sq = 1e-8;

	struct sphere {
		sphere() : center{0, 0, 0}, radius{1} {}
		sphere(point c, float r) : center{c}, radius{r} {}

		point center;
		float radius;
	};

	std::ostream& operator<<(std::ostream&, sphere const&);

	bool operator&(sphere const&, point const&);
	sphere operator|(sphere const&, sphere const&);

	bool operator==(sphere const&, sphere const&);
	bool operator&&(sphere const&, sphere const&);

	float distance(sphere const&, point const&);
	float distance(sphere const&, sphere const&);

	struct plane {
		point origin = point{0, 0, 0};
		direction dir = direction{0, 1, 0};
	};

	point closest(plane const&, point const&);

	struct segment {
		point p0, p1;

		double length() const;
	};

	float closest_param(segment const& s, point const& p, bool clamp, float eps = l_eps);
	point closest(segment const&, point const&, float eps = l_eps);
}
