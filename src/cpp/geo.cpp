#include "geo.h"

namespace ball_tree {
	sphere operator|(sphere const& a, sphere const& b) {
		double const distance = (b.center - a.center).norm();
		if (a.radius >= distance + b.radius) {
			return a;
		}
		if (b.radius >= distance + a.radius) {
			return b;
		}
		sphere c;
		c.center = (a.center + b.center) / 2;
		c.radius = std::max(a.radius, b.radius) + distance / 2;
		return c;
	}

	std::ostream& operator<<(std::ostream& os, sphere const& s) {
		return os << "S{" << s.center.x() << "," << s.center.y() << "," << s.center.z() << ";" << s.radius << "}";
	}

	bool operator&(sphere const& s, point const& p) {
		return distance(s, p) <= s.radius;
	}

	bool operator==(sphere const& s1, sphere const& s2) {
		return s1.radius == s2.radius && s1.center == s2.center;
	}

	bool operator&&(sphere const& s1, sphere const& s2) {
		return (s1.center - s2.center).norm() < s1.radius + s2.radius;
	}

	float distance(sphere const& s, point const& p) {
		return (s.center - p).norm() - s.radius;
	}

	float distance(sphere const& s1, sphere const& s2) {
		return distance(s1, s2.center) - s2.radius;
	}

	point closest(plane const& pl, point const& p) {
		auto dot = (p - pl.origin).dot(pl.dir);
		return p - dot * pl.dir;
	}

	double segment::length() const {
		return (p1 - p0).norm();
	}

	float closest_param(segment const& s, point const& p, bool clamp, float eps) {
		double l = s.length();
		double t = (s.p1 - s.p0).dot(p - s.p0) / l;
		if (l < eps) {
			eps = l * 0.2;
		}
		if (clamp) {
			if (t < l_eps) {
				t = 0;
			} else if (t > l - l_eps) {
				t = l;
			}
		} else {
			if (std::abs(t) <= l_eps) {
				t = 0;
			} else if (std::abs(l - t) <= l_eps) {
				t = l;
			}
		}
		return t;
	}

	point closest(const segment& s, const point& p, float eps) {
		double l = s.length();
		return s.p0 + (s.p1 - s.p0) * (closest_param(s, p, true, eps) / l);
	}
}

