#include "triangle.h"
#include <range/v3/algorithm/min_element.hpp>

namespace ball_tree {
	direction triangle::normal() const {
		return (b - a).cross(c - a).normalized();
	}

	triangle::operator plane() const {
		return plane{a, normal()};
	}

	std::array<float, 3> triangle::bary_coords(point const& p) const {
		using ball_tree::l_eps_sq;

		auto const s0 = b - a;
		auto const s1 = c - a;
		auto const s2 = p - a;
		double const d00 = s0.dot(s0);
		double const d01 = s0.dot(s1);
		double const d11 = s1.dot(s1);
		double const d20 = s2.dot(s0);
		double const d21 = s2.dot(s1);
		double const denom = d00 * d11 - d01 * d01;
		if (std::abs(denom) <= l_eps_sq) {
			return {{-1, -1, -1}};
		}
		float const z = (d11 * d20 - d01 * d21) / denom;
		float const y = (d00 * d21 - d01 * d20) / denom;
		return {{1 - y - z, y, z}};
	}

	std::ostream& operator<<(std::ostream& os, triangle const& t) {
		return os << "T{"                                                       //
		          << "{" << t.a.x() << ";" << t.a.y() << ";" << t.a.z() << "}," //
		          << "{" << t.b.x() << ";" << t.b.y() << ";" << t.b.z() << "}," //
		          << "{" << t.c.x() << ";" << t.c.y() << ";" << t.c.z() << "}"  //
		          << "}";                                                       //
	}

	bool operator&(triangle const& t, point const& p) {
		auto b = t.bary_coords(p);
		return b[0] >= 0 && b[1] >= 0 && (b[0] + b[1]) <= 1;
	}

	sphere circumsphere(triangle const& t) {
		sphere b;
		b.center = (t.a + t.b + t.c) / 3;
		b.radius = std::max((b.center - t.a).norm(), std::max((b.center - t.b).norm(), (b.center - t.c).norm()));
		return b;
	}

	point closest(triangle const& t, point const& p) {
		using ball_tree::segment;
		using ball_tree::vertex_closeness;
		using ball_tree::vertex_closeness_sq;

		point on_edges[3] = {
		    closest(segment{t.a, t.b}, p, vertex_closeness),
		    closest(segment{t.b, t.c}, p, vertex_closeness),
		    closest(segment{t.c, t.a}, p, vertex_closeness),
		};
		double edge_distance[3] = {
		    (p - on_edges[0]).norm(), (p - on_edges[1]).norm(), (p - on_edges[2]).norm(),
		};

		auto pos = ranges::min_element(edge_distance);
		point on_edge = on_edges[pos - &edge_distance[0]];

		point projected = closest(plane(t), p);
		if ((t & projected) && (p - on_edge).squaredNorm() > vertex_closeness_sq) {
			return p;
		}
		return on_edge;
	}
}
