#pragma once
#include "geo.h"
#include <iosfwd>
#include <vector>

namespace ball_tree {
	struct triangle {
		point a, b, c;

		direction normal() const;
		operator plane() const;
		std::array<float, 3> bary_coords(point const&) const;
	};

	std::ostream& operator<<(std::ostream&, triangle const&);

	bool operator&(triangle const&, point const&);

	sphere circumsphere(triangle const&);
	point closest(triangle const&, point const&);
}
