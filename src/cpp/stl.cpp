#include "stl.h"

namespace ball_tree {

	constexpr size_t header_size = 80;

	struct stl_triangle {
		float normal[3];
		float a[3];
		float b[3];
		float c[3];

		operator triangle() const {
			return {
			    point{a[0], a[1], a[2]}, //
			    point{b[0], b[1], b[2]}, //
			    point{c[0], c[1], c[2]}, //
			};
		}
	};

	static_assert(sizeof(stl_triangle) == 12 * sizeof(float), "unexpected padding");

	std::vector<triangle> parse_binary(std::istream& input) {
		input.ignore(header_size);

		uint32_t count;
		input.read(reinterpret_cast<char*>(&count), 4);

		std::vector<triangle> output;
		while (count-- > 0) {
			stl_triangle buffer;
			input.read(reinterpret_cast<char*>(&buffer), sizeof(buffer));
			output.push_back(buffer);

			uint16_t attrs;
			input.read(reinterpret_cast<char*>(&attrs), 2);
			input.ignore(attrs);
		}

		return output;
	}
}
