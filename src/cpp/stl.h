#pragma once
#include "triangle.h"
#include <iosfwd>
#include <vector>

namespace ball_tree {
	std::vector<triangle> parse_binary(std::istream&);
}
