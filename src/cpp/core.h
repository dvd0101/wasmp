#include <cstdint>
#include <vector>

void f();

int sumInts(int, int);

double sumDoubles(double, double);

int factorial(int);

int sumOfInts(std::vector<int> const&);

int sumOfInts(std::uintptr_t, size_t);
