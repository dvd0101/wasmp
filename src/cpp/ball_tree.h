#pragma once
#include "geo.h"

#include <gsl/span>

#include <cassert>
#include <optional>
#include <queue>
#include <range/v3/algorithm/max_element.hpp>
#include <range/v3/algorithm/reverse.hpp>
#include <range/v3/algorithm/sort.hpp>
#include <range/v3/numeric/accumulate.hpp>
#include <range/v3/view/filter.hpp>
#include <range/v3/view/slice.hpp>
#include <range/v3/view/transform.hpp>
#include <range/v3/view/zip.hpp>
#include <stack>
#include <variant>
#include <vector>

// clang-format off
template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;
// clang-format on

namespace ball_tree {

	namespace details {

		enum class span_coordinate { x = 0, y = 1, z = 2 };

		/* max_span_coordinate returns the coordinate with the max span among
		 * all the spheres.
		 */
		span_coordinate max_span_coordinate(gsl::span<sphere const>);

		sphere compute_union_ball(gsl::span<sphere const>);
	}

	namespace details {
		static int const empty = -1;

		struct tree_node {
			sphere ball;
			int parent = empty;
		};

		struct leaf_node : tree_node {
			int first_leaf = empty;
			int last_leaf = empty;
		};

		struct branch_node : tree_node {
			int left_branch = empty;
			int right_branch = empty;
		};

		using node = std::variant<leaf_node, branch_node>;

		inline sphere const& get_ball(node const& n) {                               //
			return std::visit([](auto& el) -> sphere const& { return el.ball; }, n); //
		}                                                                            //

		template <typename PriorityQueue>
		void discard(PriorityQueue& queue, int n) {
			while (n--) {
				queue.pop();
			}
		}

		template <typename PriorityQueue>
		auto to_vector(PriorityQueue& queue) {
			std::vector<typename PriorityQueue::value_type> output;
			while (!queue.empty()) {
				output.push_back(queue.top());
				queue.pop();
			}
			return output;
		}
	}

	template <typename T>
	class static_index {
	  public:
		template <template <typename, typename...> typename Container>
		static_index(Container<T> elements)
		    : elements_{std::move(elements)},                                                          //
		      balls_{ranges::view::transform(elements_, [](T const& v) { return circumsphere(v); })} { //
			index_elements();
		}

	  public:
		size_t size() const {
			return tree_.size();
		}

	  public:
		struct query_result {
			T value;
			point closest_point;
			float distance;

			bool operator<(query_result const& b) const {
				return distance < b.distance;
			}
		};

	  private:
		// a queue where the top() element is the farthest one
		using farthest_queue = std::priority_queue<query_result, std::deque<query_result>, std::less<query_result>>;

	  public:
		std::vector<query_result>
		    nearest_neighbors(point const& p, size_t k, float max_distance = std::numeric_limits<float>::infinity()) const {
			if (tree_.empty()) {
				return {};
			}

			using details::leaf_node;
			using details::branch_node;

			farthest_queue results;

			sphere query{p, std::numeric_limits<float>::infinity()};
			std::stack<int> stack;
			stack.push(0);
			while (!stack.empty()) {
				int const node_index = stack.top();
				stack.pop();
				auto& node = tree_[node_index];
				auto const& node_ball = get_ball(node);
				if (!(node_ball && query)) {
					continue;
				}
				std::visit(
				    overloaded{
				        [&](leaf_node const& n) {
					        if (!(get_ball(n) && query)) {
						        return;
					        }
					        add_leaf_elements(n, p, max_distance, results);
					        if (results.size() > k) {
								details::discard(results, results.size() - k);
								query.radius = results.top().distance;
					        }
					    },
				        [&](branch_node const& n) { select_branch_nodes(n, query, stack); },
				    },
				    node);
			}

			std::vector<query_result> output = details::to_vector(results);
			ranges::reverse(output);
			return output;
		}

	  private:
		void
		    add_leaf_elements(details::leaf_node const& node, point const& p, float max_distance, farthest_queue& results) const {
			using namespace ranges;
			// clang-format off
			auto elems = view::slice(elements_, node.first_leaf, node.last_leaf)
				| view::transform([&p](T const& el) {
					query_result r;
					r.value = el;
					r.closest_point = closest(el, p);
					r.distance = (r.closest_point - p).norm();
					return r;
				})
				| view::filter([max_distance](auto const& result) { return result.distance <= max_distance; });
			// clang-format on
			for (auto const& e : elems) {
				results.push(e);
			}
		}

		void select_branch_nodes(details::branch_node const& node, sphere const& query, std::stack<int>& stack) const {
			using details::empty;
			float inf = std::numeric_limits<float>::infinity();
			std::array<std::pair<float, int>, 2> distances{{{inf, empty}, {inf, empty}}};

			auto it = distances.begin();
			for (auto branch_index : {node.left_branch, node.right_branch}) {
				if (branch_index != empty) {
					sphere ball = get_ball(tree_[branch_index]);
					if (ball && query) {
						it->first = distance(ball, query.center);
						it->second = branch_index;
					}
				}
				it++;
			}

			ranges::sort(distances, [](auto& a, auto& b) { return a.first > b.first; });
			for (auto & [ _, branch_index ] : distances) {
				if (branch_index != empty) {
					stack.push(branch_index);
				}
			}
		}

	  private:
		void index_elements() {
			kd_construction(-1, 0, elements_.size());
		}

		int kd_construction(int parent, int first, int last) {
			using details::leaf_node;
			using details::branch_node;
			assert(last > first);

			auto balls = gsl::span<sphere>(balls_).subspan(first, last - first);
			if (last - first <= 50) {
				tree_.push_back(leaf_node{
				    {details::compute_union_ball(balls), parent}, first, last,
				});
				return tree_.size() - 1;
			}

			using namespace ranges;
			auto const coord = static_cast<int>(details::max_span_coordinate(balls));
			sort(view::zip(view::slice(elements_, first, last), balls),
			     [coord](auto const& a, auto const& b) { return a.second.center[coord] < b.second.center[coord]; });

			int const split_point = (last + first) / 2;

			tree_.emplace_back(branch_node{});
			int const node_index = tree_.size() - 1;

			branch_node new_node;
			new_node.parent = parent;
			new_node.left_branch = kd_construction(node_index, first, split_point);
			new_node.right_branch = kd_construction(node_index, split_point, last);
			new_node.ball = get_ball(tree_[new_node.left_branch]) | get_ball(tree_[new_node.right_branch]);

			tree_[node_index] = new_node;
			return node_index;
		}

	  private:
		std::vector<T> elements_;
		std::vector<sphere> balls_;
		std::vector<details::node> tree_;
	};
}
