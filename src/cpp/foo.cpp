#include "test_ball_tree.h"
#include <fstream>
#include <iostream>

int main() {
	using namespace test;
 	std::ifstream f("./test.stl");
 	auto triangles = stl::parse_binary(f);
 	ball_tree::static_index<triangle> index(triangles);
 	std::cout << "elements: " << triangles.size() << "\n";
 	std::cout << "index nodes: " << index.size() << "\n";

 	point query{108.109 - 0.01, -50.453, -1.91634};
 	auto results = index.nearest_neighbors(query, 5);
 	std::cout << "query results: " << results.size() << "\n";
 	for (int ix = 0; ix < results.size(); ix++) {
 		auto& r = results[ix];
 		std::cout << r.value << "\n\n";
 	}
}
