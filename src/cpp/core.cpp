#include "core.h"
#include <iostream>
#include <numeric>
#include <functional>
#include <vector>

void f() {}

int sumInts(int a, int b) { return a + b; }

double sumDoubles(double a, double b) { return a + b; }

int factorial(int n) {
  return n * (n > 1 ? factorial(n-1) : 1);
};

int sumOfInts(std::vector<int> const& data) {
	return std::accumulate(data.begin(), data.end(), 0, std::plus{});
}

int sumOfInts(std::uintptr_t ptr, size_t size) {
	auto first = reinterpret_cast<int const*>(ptr);
	auto last = first + size;
	return std::accumulate(first, last, 0, std::plus{});
}
