#include "ball_tree.h"
#include "core.h"
#include "stl.h"

#include <string>

#include <emscripten/bind.h>

template <typename Vector>
void copy_js_typed_array(emscripten::val const& typed_array, Vector& vec) {
	// see https://github.com/kripken/emscripten/issues/5519#issuecomment-333302296
	using emscripten::val;

	auto size = typed_array["length"].as<size_t>();
	vec.resize(size);

	val memory = val::module_property("buffer");
	val memoryView = typed_array["constructor"].new_(memory, reinterpret_cast<uintptr_t>(vec.data()), size);
	memoryView.call<void>("set", typed_array);
}

int sumOfInts(emscripten::val const& typed_array) {
	std::vector<int> values;
	copy_js_typed_array(typed_array, values);
	return sumOfInts(values);
}

std::vector<ball_tree::triangle> parseSTL(std::uintptr_t ptr, size_t size) {
	auto begin = reinterpret_cast<char*>(ptr);

	struct vector_buffer : public std::basic_streambuf<char> {
		vector_buffer(char* ptr, size_t n) {
			setg(ptr, ptr, ptr + n);
		}
	};

	vector_buffer buff(begin, size);
	std::istream stream(&buff);
	return ball_tree::parse_binary(stream);
}

using ball_tree::point;
using ball_tree::triangle;
using static_index_triangle = ball_tree::static_index<triangle>;

static_index_triangle buildStaticIndexFromSTL(std::uintptr_t ptr, size_t size) {
	return static_index_triangle(parseSTL(ptr, size));
}

using namespace emscripten;

EMSCRIPTEN_BINDINGS(bench_cpp) {
	function("f", &f);
	function("sumInts", &sumInts);
	function("sumDoubles", &sumDoubles);
	function("factorial", &factorial);

	register_vector<int>("VectorInt");

	function("sumOfInts", select_overload<int(std::vector<int> const&)>(&sumOfInts));
	function("sumOfIntsArray", select_overload<int(std::uintptr_t, size_t)>(&sumOfInts));
	function("sumOfIntsJSArray", select_overload<int(emscripten::val const&)>(&sumOfInts));

	value_object<point>("Point")                        //
	    .field("x",                                     //
	           +[](point const& p) { return p.x(); },   //
	           +[](point& p, double v) { p.x() = v; })  //
	    .field("y",                                     //
	           +[](point const& p) { return p.y(); },   //
	           +[](point& p, double v) { p.y() = v; })  //
	    .field("z",                                     //
	           +[](point const& p) { return p.z(); },   //
	           +[](point& p, double v) { p.z() = v; }); //

	class_<triangle>("Triangle")      //
	    .property("a", &triangle::a)  //
	    .property("b", &triangle::b)  //
	    .property("c", &triangle::c); //

	register_vector<triangle>("VectorTriangle");

	function("parseSTL", &parseSTL);

	value_object<static_index_triangle::query_result>("StaticIndexResult")           //
	    .field("value", &static_index_triangle::query_result::value)                 //
	    .field("closest_point", &static_index_triangle::query_result::closest_point) //
	    .field("distance", &static_index_triangle::query_result::distance);          //

	register_vector<static_index_triangle::query_result>("VectorStaticIndexResult");

	class_<static_index_triangle>("StaticIndexTriangle")                           //
	    .function("size", &static_index_triangle::size)                            //
	    .function("nearest_neighbors", &static_index_triangle::nearest_neighbors); //

	function("buildStaticIndexFromSTL", &buildStaticIndexFromSTL);
}
