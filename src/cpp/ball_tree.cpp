#include "ball_tree.h"
#include <algorithm>
#include <range/v3/algorithm/max_element.hpp>
#include <range/v3/numeric/accumulate.hpp>

namespace ball_tree {
	namespace details {
		span_coordinate max_span_coordinate(gsl::span<sphere const> balls) {
			if (balls.size() == 0) {
				return span_coordinate::x;
			}
			auto ball = balls.begin();
			double mins[3] = {ball->center.x(), ball->center.y(), ball->center.z()};
			double maxs[3] = {ball->center.x(), ball->center.y(), ball->center.z()};
			for (++ball; ball != balls.end(); ++ball) {
				for (int c = 0; c < 3; c++) {
					if (ball->center[c] - ball->radius < mins[c]) {
						mins[c] = ball->center[c] - ball->radius;
					}
					if (ball->center[c] + ball->radius > maxs[c]) {
						maxs[c] = ball->center[c] + ball->radius;
					}
				}
			}
			double const spans[3] = {maxs[0] - mins[0], maxs[1] - mins[1], maxs[2] - mins[2]};
			auto max_span = ranges::max_element(spans);
			return static_cast<span_coordinate>(max_span - std::begin(spans));
		}

		sphere compute_union_ball(gsl::span<sphere const> balls) {
			return ranges::accumulate(balls, balls[0], std::bit_or{});
		}
	}
}
