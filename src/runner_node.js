import { Cli } from './reporter';
import { run } from './bench';
import { collectSuites } from './suites/index';
import { _globals } from './context';


const jsMod = require('./bench_js.js');
const cppMod = require('./bench_cpp.js');

setTimeout(() => {
  _globals.cpp = cppMod;
  _globals.js = jsMod;
  collectSuites()
    .then((suites) => {
      run(new Cli(), suites);
    });
}, 1000);

