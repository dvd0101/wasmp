import { isBrowser } from './context';

function connect(suite, reporter) {
  suite.forEach((b) => {
    b.on('start', evt => reporter.benchStart(suite, evt.target));
  });
  suite.on('cycle', evt => reporter.addResults(suite, evt.target));
  suite.on('error', (evt) => {
    const bench = evt.target;
    console.log('>>> error', bench.name, bench.error);
    reporter.benchError(suite, bench)
  });
  suite.on('start', () => reporter.suiteStart(suite));
  suite.on('complete', () => reporter.report(suite));
}

export function run(reporter, suites) {
  let index = 0;
  return new Promise((resolve) => {
    const runOne = () => {
      if (index === suites.length) {
        resolve();
        return;
      }
      const suite = suites[index];
      index += 1;
      connect(suite, reporter);
      suite.on('complete', () => runOne());
      suite.run({ async: isBrowser });
    };
    runOne();
  });
}

