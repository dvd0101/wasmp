import Benchmark from 'benchmark';
import { _globals } from '../context';

const _empty = () => {};

export const emptyFn = () => {
  const suite = new Benchmark.Suite('empty function');
  suite.add('js', () => { _globals.js.f(); });
  suite.add('js (inline)', () => { _empty(); });
  suite.add('c++', () => { _globals.cpp.f(); });

  return suite;
};

export const sumInts = () => {
  const suite = new Benchmark.Suite('sum two numbers');
  suite.add('js', () => _globals.js.sumNumbers(1, 2));
  suite.add('c++ (int + int)', () => _globals.cpp.sumInts(1, 2));
  suite.add('c++ (double + double)', () => _globals.cpp.sumDoubles(1, 2));

  return suite;
};

const _factorial = function (n) {
  return n * (n > 1 ? _factorial(n - 1) : 1);
};

export const factorial = () => {
  const suite = new Benchmark.Suite('factorial(10)');
  suite.add('js', () => _globals.js.factorial(10));
  suite.add('js (inline)', () => _factorial(10));
  suite.add('c++', () => _globals.cpp.factorial(10));

  return suite;
};
