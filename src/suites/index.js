import * as core from './core';
import * as list from './list';
import * as ballTree from './ball_tree';

const modules = [
  core,
  ballTree,
  list,
];

export const collectSuites = (() => {
  const promises = [];
  for (const mod of modules) {
    promises.push(...Object.keys(mod).map(attr => Promise.resolve(mod[attr]())));
  }
  return Promise.all(promises);
});
