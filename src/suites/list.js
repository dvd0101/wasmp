/* eslint-disable no-underscore-dangle, no-plusplus */
import Benchmark from 'benchmark';
import { _globals } from '../context';
import { cppTypedArray, releaseMemory } from '../utils';

let _intList;
let _intVector;
let _intArrayBuffer;
let _intArrayBufferWASM;

function intList(n) {
  const l = [];
  for (let ix = 0; ix < n; ix++) { l.push(ix); }
  return l;
}

function sharedIntList() {
  if (!_intList) {
    _intList = intList(1000);
  }
  return _intList;
}

function intVector(n) {
  const v = new _globals.cpp.VectorInt();
  for (let ix = 0; ix < n; ix++) { v.push_back(ix); }
  return v;
}

function sharedVectorInt() {
  if (!_intVector) {
    _intVector = intVector(1000);
  }
  return _intVector;
}

function intArrayBuffer(n) {
  const b = new Int32Array(n);
  for (let ix = 0; ix < n; ix++) { b[ix] = ix; }
  return b;
}

function sharedArrayBuffer() {
  if (!_intArrayBuffer) {
    _intArrayBuffer = intArrayBuffer(1000);
  }
  return _intArrayBuffer;
}

function intArrayBufferWASM(n) {
  const b = cppTypedArray(Int32Array, n);
  for (let ix = 0; ix < n; ix++) { b[ix] = ix; }
  return b;
}

function sharedArrayBufferWASM() {
  if (!_intArrayBufferWASM) {
    _intArrayBufferWASM = intArrayBufferWASM(1000);
  }
  return _intArrayBufferWASM;
}


export const sumOfPreallocatedInts = () => {
  const suite = new Benchmark.Suite('sum of pre allocated integers');
  suite.add('sum of 1000 integers using a list [js]', () => {
    const r = _globals.js.sumOfInts(sharedIntList());
    if (r !== 499500) throw new Error();
  });

  suite.add('sum of 1000 integers using list.reduce [js]', () => {
    const r = _globals.js.sumOfIntsSlow(sharedIntList());
    if (r !== 499500) throw new Error();
  });

  suite.add('sum of 1000 integers using an array buffer [js]', () => {
    const r = _globals.js.sumOfIntsBuffer(sharedArrayBuffer());
    if (r !== 499500) throw new Error();
  });

  suite.add('sum of 1000 integers using a vector [c++]', () => {
    const r = _globals.cpp.sumOfInts(sharedVectorInt());
    if (r !== 499500) throw new Error();
  });

  suite.add('sum of 1000 integers using an array buffer on WASM memory [c++]', () => {
    const b = sharedArrayBufferWASM();
    const r = _globals.cpp.sumOfIntsArray(b.ptr, b.length);
    if (r !== 499500) throw new Error();
  });

  suite.add('sum of 1000 integers using an array buffer on JS memory [c++]', () => {
    const r = _globals.cpp.sumOfIntsJSArray(sharedArrayBuffer());
    if (r !== 499500) throw new Error();
  });
  return suite;
};

// export const listPrepare = () => {
//   const suite = new Benchmark.Suite('prepare a container with 1000 integers');
//   suite.add('Array', () => intList(1000));
//   suite.add('ArrayBuffer', () => intArrayBuffer(1000));
//   suite.add('ArrayBuffer on WASM memory ', () => releaseMemory(intArrayBufferWASM(1000)));
//   suite.add('std::vector', () => intVector(1000).delete());
//   return suite;
// };

export const updateMemory = () => {
  const suite = new Benchmark.Suite('update a list of integers');

  _globals.run = {
    intList: intList(1000),
    intVector: intVector(1000),
    intArrayBuffer: intArrayBuffer(1000),
    intArrayBufferWASM: intArrayBufferWASM(1000),
  };

  suite.add({
    name: 'update a list of 1000 integers',
    fn: () => {
      const l = _globals.run.intList;
      for (let ix = 0; ix < l.length; ix++) {
        l[ix] = ix;
      }
    },
  });

  suite.add({
    name: 'update a vector of 1000 integers',
    fn: () => {
      const l = _globals.run.intVector;
      for (let ix = 0; ix < l.size(); ix++) {
        l[ix] = ix;
      }
    },
  });

  suite.add({
    name: 'update an array buffer of 1000 integers',
    fn: () => {
      const l = _globals.run.intArrayBuffer;
      for (let ix = 0; ix < l.length; ix++) {
        l[ix] = ix;
      }
    },
  });

  suite.add({
    name: 'update an array buffer on WASM memory of 1000 integers',
    fn: () => {
      const l = _globals.run.intArrayBufferWASM;
      for (let ix = 0; ix < l.length; ix++) {
        l[ix] = ix;
      }
    },
  });

  return suite;
};
