/* eslint-disable import/prefer-default-export, prefer-destructuring */
import Benchmark from 'benchmark';
import { _globals } from '../context';
import { cppTypedArray, releaseMemory } from '../utils';
import { readFile } from 'readFile';

let _jsStaticIndex;
let _cppStaticIndex;
let _randomPoints;

let _buildPromise;

function _buildIndex() {
  if (_buildPromise) {
    return _buildPromise;
  }
  _buildPromise = readFile('../test.stl')
    .then((buffer) => {
      const stl = cppTypedArray(Uint8Array, buffer.byteLength);
      stl.set(new Uint8Array(buffer));

      _cppStaticIndex = _globals.cpp.buildStaticIndexFromSTL(stl.ptr, stl.length);

      const vt = _globals.cpp.parseSTL(stl.ptr, stl.length);
      const triangles = [];

      const Triangle = _globals.js.Triangle;
      const Point = _globals.js.Point;
      for (let ix = 0; ix < vt.size(); ix += 1) {
        const t = vt.get(ix);
        triangles.push(new Triangle(
          new Point(t.a.x, t.a.y, t.a.z),
          new Point(t.b.x, t.b.y, t.b.z),
          new Point(t.c.x, t.c.y, t.c.z),
        ));
      }

      _randomPoints = [];
      for (let ix = 0; ix < 1000; ix += 1) {
        const p = Math.floor(Math.random() * triangles.length);
        const coord = triangles[p].a.toArray();
        coord[0] += Math.random() / 100;
        coord[1] += Math.random() / 100;
        coord[2] += Math.random() / 100;
        _randomPoints.push(coord);
      }

      releaseMemory(buffer);
      vt.delete();

      _jsStaticIndex = new _globals.js.StaticIndex(triangles);
    });
  return _buildPromise;
}

function getJsStaticIndex() {
  if (!_jsStaticIndex) {
    return _buildIndex().then(() => _jsStaticIndex);
  }
  return Promise.resolve(_jsStaticIndex);
}

function getCppStaticIndex() {
  if (!_cppStaticIndex) {
    return _buildIndex().then(() => _cppStaticIndex);
  }
  return Promise.resolve(_cppStaticIndex);
}

function getRandomPoints() {
  if (!_randomPoints) {
    return _buildIndex().then(() => _randomPoints);
  }
  return Promise.resolve(_randomPoints);
}

export const ballTreeSinglePoint = function () {
  return Promise.all([getCppStaticIndex(), getJsStaticIndex()])
    .then(([cppStaticIndex, jsStaticIndex]) => {
      const suite = new Benchmark.Suite('query one known point');

      suite.add('js', () => {
        jsStaticIndex.nearestNeighbors(new _globals.js.Point(108.109 - 0.01, -50.453, -1.91634), 1);
      });

      suite.add('c++', () => {
        const r = cppStaticIndex.nearest_neighbors({ x: 108.109 - 0.01, y: -50.453, z: -1.91634 }, 1, Infinity);
        r.delete();
      });

      return suite;
    });
};

export const ballTreeSinglePointAndItsNeighbors = function () {
  return Promise.all([getCppStaticIndex(), getJsStaticIndex()])
    .then(([cppStaticIndex, jsStaticIndex]) => {
      const suite = new Benchmark.Suite('query a known point for its 1000 neighbors');

      suite.add('js', () => {
        jsStaticIndex.nearestNeighbors(new _globals.js.Point(108.109 - 0.01, -50.453, -1.91634), 1000);
      });

      suite.add('c++', () => {
        const r = cppStaticIndex.nearest_neighbors({ x: 108.109 - 0.01, y: -50.453, z: -1.91634 }, 1000, Infinity);
        r.delete();
      });

      return suite;
    });
};

export const ballTree1000RandomPoints = function () {
  return Promise.all([getCppStaticIndex(), getJsStaticIndex(), getRandomPoints()])
    .then(([cppStaticIndex, jsStaticIndex, randomPoints]) => {
      const suite = new Benchmark.Suite('query 1000 random points');

      suite.add('js', () => {
        for (let ix = 0; ix < randomPoints.length; ix += 1) {
          const p = new _globals.js.Point(...randomPoints[ix]);
          jsStaticIndex.nearestNeighbors(p, 1);
        }
      });

      suite.add('c++', () => {
        for (let ix = 0; ix < randomPoints.length; ix += 1) {
          const p = {
            x: randomPoints[ix][0],
            y: randomPoints[ix][1],
            z: randomPoints[ix][2],
          };
          const r = cppStaticIndex.nearest_neighbors(p, 1, Infinity);
          r.delete();
        }
      });

      return suite;
    });
};
