export function readFile(fpath) {
  return fetch(fpath)
    .then(response => response.arrayBuffer());
}

