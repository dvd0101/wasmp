let _isBrowser = true;
try {
  const w = window;
} catch (e) {
  _isBrowser = false;
}

export const isBrowser = _isBrowser;

export const _globals = {};

