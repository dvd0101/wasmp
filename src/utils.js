import { _globals } from './context';

/**
 * cppTypedArray returns a TypedArray allocated on the cpp heap.
 *
 * @param constructor The constructor of the wanted typed array.
 * @param size The size of the typed array.
 *
 * @return The return value is a typed array with an additional `ptr`
 * attribute; `ptr` is the "pointer" to the cpp heap where the typedarray
 * starts.
 *
 * example:
 *
 *    const t = cppTypedArray(Float64Array, 100)
 */

export function cppTypedArray(constructor, size) {
  const bytes = size * constructor.BYTES_PER_ELEMENT;
  const ptr = _globals.cpp._malloc(bytes);

  const cppMemory = _globals.cpp.HEAPU8.subarray(ptr, ptr + bytes);
  const ta = new constructor(cppMemory.buffer, cppMemory.byteOffset, size);
  ta.ptr = ptr;
  return ta;
}

/**
 * releaseMemory releases the memory from the cpp heap previously allocated by
 * calling (directly or indirectly) the _malloc function.
 *
 * @param o The pointer to release
 */
export function releaseMemory(o) {
  const ptr = typeof o === 'object' ? o.ptr : o;
  _globals.cpp._free(ptr);
}
