const fs = require('fs');

export function readFile(fpath) {
  return new Promise((resolve) => {
    fs.readFile(fpath, (err, data) => {
      if (err) {
        throw err;
      }
      resolve(data);
    });
  });
}
