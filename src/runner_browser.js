import { Cli } from './reporter_html';
import { run } from './bench';
import { collectSuites } from './suites/index';
import { _globals } from './context';

export function start() {
  setTimeout(() => {
    _globals.cpp = window.Module;
    _globals.js = window.bench_js;
    collectSuites()
      .then((suites) => {
        run(new Cli(), suites);
      });
  }, 1000);
}
