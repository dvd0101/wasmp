function addCell(text, opts) {
  const cfg = {
    tag: 'td',
    className: '',
    ...opts,
  };
  const td = document.createElement(cfg.tag);
  td.innerText = text;
  td.className = cfg.className;
  return td;
}

function newSuite(name) {
  const table = document.createElement('table');

  const thead = document.createElement('thead');

  const header = document.createElement('tr');

  const title = addCell(name, {tag: 'th'});
  title.colSpan = 3;
  header.appendChild(title);
  thead.appendChild(header);

  const header2 = document.createElement('tr');
  header2.appendChild(addCell('benchmark', {tag: 'th'}));
  header2.appendChild(addCell('ops/sec', {tag: 'th'}));
  header2.appendChild(addCell('deviation', {tag: 'th'}));
  thead.appendChild(header2);

  table.appendChild(thead);

  const tbody = document.createElement('tbody');
  table.appendChild(tbody);

  const r = document.getElementById('results');
  r.appendChild(table);

  return tbody;
}

function createRow(suite, name) {
  const tr = document.createElement('tr');
  tr.appendChild(addCell(name));
  tr.appendChild(addCell('...'));
  tr.appendChild(addCell('...'));

  suite.appendChild(tr);
  return tr;
}

const fmt = new Intl.NumberFormat('en-us');

function updateRow(row, data) {
  row.childNodes[0].innerText = data.name;
  row.childNodes[1].innerText = fmt.format(data.ops);
  row.childNodes[2].innerText = `±${data.deviation}%`;
}

export class Cli {
  constructor() {
    this._suites = new Map();
  }

  suiteStart(suite) {
    const data = {
      table: newSuite(suite.name),
      rows: new Map(),
      results: [],
    };
    this._suites.set(suite, data);
  }

  benchStart(suite, bench) {
    const data = this._suites.get(suite);
    data.rows.set(bench, createRow(data.table, bench.name));
  }

  benchError(suite, bench) {
    const data = this._suites.get(suite);
    const tr = data.rows.get(bench);
    tr.className += ' fail';
    tr.childNodes[0].innerText = `${bench.name} (${bench.error})`;
  }

  addResults(suite, bench) {
    const data = {
      name: bench.name,
      hz: bench.hz,
      ops: bench.hz.toFixed(bench.hz < 100 ? 2 : 0),
      deviation: bench.stats.rme.toFixed(2),
    };
    const suiteData = this._suites.get(suite);
    const tr = suiteData.rows.get(bench);
    updateRow(tr, data);
    if (bench.stats.rme > 3) {
      tr.className += ' high-deviation';
    }
    suiteData.results.push([tr, data]);
  }

  report(suite) {
    const suiteData = this._suites.get(suite);
    let best;
    let maxOps = 0;
    for (let ix = 0; ix < suiteData.results.length; ix += 1) {
      const [row, data] = suiteData.results[ix];
      if (data.hz > maxOps) {
        best = row;
        maxOps = data.hz;
      }
    }
    best.className += ' best';
    this._suites.delete(suite);
  }
}
