const clc = require('cli-color');
const benchmarks = require('beautify-benchmark');

export class Cli {
  constructor() {
    this.error = clc.red;
    this.notice = clc.blue;
  }

  suiteStart(suite) {
    console.log(this.notice(`running: ${suite.name}`));
  }

  benchStart(suite, bench) {
    // console.log(this.notice(`> ${bench.name}`));
  }

  benchError(suite, bench) {
    console.log(this.error(`! ${bench.name} -- ${bench.error}`));
  }

  addResults(suite, bench) {
    benchmarks.add(bench);
  }

  report(suite) {
    benchmarks.log({ reset: true });
  }
}
