# WASM/JS benchmark playground

## using cmake to build the c++ bits

	$ mkdir build; cd build
	$ cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=${EMSCRIPTEN_HOME}/cmake/Modules/Platform/Emscripten.cmake ..
	$ make && make install

## using webpack to build the js bits

	$ npm install
	$ ./node_modules/.bin/webpack --config webpack.node.config.js -p
	$ ./node_modules/.bin/webpack --config webpack.browser.config.js -p

## run the benchmarks on node >= 9

	$ cd dist
	$ node runner.js

## run the benchmarks on a browser

	$ cd dist
	$ python -mSimpleHTTPServer
