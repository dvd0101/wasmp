const path = require('path');

module.exports = {
  entry: {
    runner: './src/runner_node.js',
    bench_js: './src/js/index.js',
  },
  target: 'node',
  output: {
    filename: '[name].js',
    libraryTarget: 'commonjs2',
  },
  resolve: {
    alias: {
      readFile$: path.resolve(__dirname, 'src/read_file_node.js'),
    },
  },
  externals: [
    (context, request, callback) => {
      if (/bench_js.js$/.test(request) || /bench_cpp.js$/.test(request)) {
        return callback(null, `commonjs ${request}`);
      }
      callback();
    },
  ],
};
